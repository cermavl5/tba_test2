from fastapi import FastAPI
from fastapi.responses import HTMLResponse
import pandas as pd
import uvicorn
from pathlib import Path
from random import randint
import os
from urllib.request import urlretrieve


app = FastAPI()
html_file_path = Path(__file__).parent.joinpath('static').joinpath('index.html')# Define html index path

@app.get("/", response_class=HTMLResponse)
def root():        
    with open(html_file_path, 'r') as file:
        content= file.read()
    return HTMLResponse(content=content,status_code=200)

@app.get("/generate/{filename}/{number}")
def generate(filename:str,number:int):
    # Function for generating random numbers and saving to the file
    with open("numbers.txt", "w") as file:
        file.writelines(f"{randint(1, 100)}\n" for _ in range(100))

    return ""

@app.get("/getfiles/")
def all_subjects():
    # Method for returning all file names from /data folder
    files = [f for f in os.listdir() if os.path.isfile(f)]

    return files

@app.get("/getfile/{filename}")
def subject(id:int):
    # Method for returning file to download
    url = ("")
    filename = "file.txt"

    return urlretrieve(url, filename)